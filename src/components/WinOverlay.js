import React from 'react';

class WinOverlay extends React.Component {

    render() {
        return(
            <div className="win-overlay">
                <h2>You win!</h2>
                <div>
                    <button className="button" onClick={this.props.finishLevel}>
                        Go back to menu
                    </button>
                </div>
            </div>
        );
    }

}

export default WinOverlay;