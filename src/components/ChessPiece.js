import React from 'react';

class ChessPiece extends React.Component {

    static generatePiece(isWhite, pieceType) {
        return pieceType === "o" ? null
            : {isWhite: isWhite, pieceType: pieceType}
    };

    render() {
        const spriteSize = -64;
        const imageY = this.props.piece.isWhite ? 0 : 1;
        const imageX = ["K", "Q", "B", "N", "R", "i"].indexOf(this.props.piece.pieceType);

        const style = this.props.isHeld ? null : {
            backgroundImage: "url('pieces-64.png')",
            backgroundRepeat: "no-repeat",
            backgroundPosition: (imageX * spriteSize) + "px " + (imageY * spriteSize) + "px",
        };

        if (this.props.ghost) {
            style.opacity = 0.1;
        }

        const className = this.props.isHeld ? "piece hl" : "piece";

        return (
            <div style={style} className={className}>
                {/*this.props.piece.pieceType*/}
            </div>
        );
    }
}

export default ChessPiece;