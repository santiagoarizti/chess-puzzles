import React from 'react';
import Board from "./Board";

class Navigation extends React.Component {

    handleClick(offset) {
        this.props.navigate(this.props.currentStep + offset);
    }

    static renderDescription (step, i, currentStep, length) {
        const isCurrent = i === currentStep;
        const isLast = i === length - 1;

        // means the game just started and there is no move.
        if (typeof step.clickedSquare !== "number") {
            return null;
        }

        const iconMap = [
            {"R": "♖", "B": "♗", "Q": "♕", "K": "♔", "N": "♘", "i": "♙"}, // white
            {"R": "♜", "B": "♝", "Q": "♛", "K": "♚", "N": "♞", "i": "♟"}  // black
        ];
        // for reference, the icons above have html entities as in the example below:
        /*const iconHtmlMap = [
            {"R": "&#9814;", "B": "&#9815;", "Q": "&#9813;", "K": "&#9812;", "N": "&#9816;", "i": "&#9817;"}, // white
            {"R": "&#9820;", "B": "&#9821;", "Q": "&#9819;", "K": "&#9818;", "N": "&#9822;", "i": "&#9823;"}  // black
        ];*/

        const notation = Board.getChessCoordinates(step.clickedSquare);
        const icon = iconMap[0][step.landingPiece.pieceType];
        return (
            <span className={isCurrent ? 'current' : ''} key={i}>
                {icon}x{notation.col}{notation.row} {isLast ? "" : ", "}
            </span>
        );
    }

    render() {

        const disablePrev = this.props.currentStep <= 0;
        const disableNext = this.props.currentStep >= this.props.totalMoves - 1;

        const moves = this.props.history
            .map((step, i) => Navigation.renderDescription(step, i, this.props.currentStep, this.props.history.length))
            .filter(element => element !== null); // the first entry is empty.

        return (
            <div className="navigation">
                <button disabled={disablePrev} className="button"
                    onClick={() => this.handleClick(-1)}>&lt;=</button>
                <div className="moves">
                    <div>{moves}</div>
                </div>
                <button disabled={disableNext} className="button"
                    onClick={() => this.handleClick(1)}>=&gt;</button>
            </div>
        );

    }

}

export default Navigation;