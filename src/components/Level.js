import React from 'react';
import Board from './Board';
import ChessPiece from './ChessPiece';
import Navigation from './Navigation';
import PieceCalculator from '../PieceCalculator';
import levels from '../levels';
import WinOverlay from './WinOverlay';

class Level extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Level.getStartingBoard(this.props.level),
                clickedSquare: null, // i of square
            }],
            stepNumber: 0,
            heldPiece: null, // can have i of square + piece
        };

        // this should not be part of the state, it changes too often.
        this.mouse = {x: "0px", y: "0px"};

        // on mouse move, keep the floating piece next to the mouse if there is one.
        const _this = this;
        window.addEventListener("mousemove", function(e) {
            Level.trackMovement(e.pageX, e.pageY, _this);
        });

        window.addEventListener("touchmove", function(e) {
            const touch = e.touches[e.touches.length - 1];
            Level.trackMovement(touch.clientX, touch.clientY, _this);
            e.preventDefault();
        });
    }

    static trackMovement(pageX, pageY, _this) {
        // for the initial position of the floating piece.
        _this.mouse.x = (pageX - 32) + "px";
        _this.mouse.y = (pageY - 64 - 1) + "px";
        const floating = document.querySelector("[data-floating-piece]");
        if (floating) {
            // for the live position of the floating piece
            floating.style.left = _this.mouse.x;
            floating.style.top = _this.mouse.y;
        }
    }

    static getStartingBoard(level) {
        const levelData = levels[level];
        const rawPieces = levelData.pieces.split("");
        return rawPieces.map(p => ChessPiece.generatePiece(true, p));
    }

    handleTouch(e, i, piece) {
        //const touch = e.touches[e.touches.length - 1];
        const touch = e.changedTouches[0];
        const x = touch ? touch.clientX : this.mouse.x;
        const y = touch ? touch.clientY : this.mouse.y;
        const realElement = document.elementFromPoint(x, y);
        Level.trackMovement(x, y, this);
        realElement.click();
        //this.handleClick(i, piece);
    }

    handleClick(i, piece) {
        let history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();

        let heldPiece = this.state.heldPiece; // square + piece
        let newHeldPiece = null; // the heldPiece for the next click
        let stepNumber = history.length - 1; // not sure if we will advance.

        // helper functions. todo: maybe move to own function.
        const holdPiece = () => {
            newHeldPiece = {i: i, piece: piece};
        };
        const movePieceToSquare = () => {
            // remove piece from old location.
            squares[heldPiece.i] = null;
            // put piece in new location
            squares[i] = heldPiece.piece;
            // advance step number for history keeping
            stepNumber++;
            // add to history
            history = history.concat([{
                squares: squares,
                clickedSquare: i,
                landingPiece: heldPiece.piece
            }]);
        };

        if (heldPiece) {
            if (!piece || heldPiece.i === i) {
                // square is empty, not allowed in this type of chess.
                // or square is the same as it started, in that case then ignore the move.
            } else {
                // before moving the piece, lets make sure it was a possibility.
                const possibilities = PieceCalculator.calculatePossibleSquares(heldPiece.i, heldPiece.piece.pieceType, squares);
                if (possibilities[i]) {
                    movePieceToSquare();
                }
            }
        } else {
            if (piece) {
                holdPiece();
            } else {
                // no held piece, and no holdable piece, just ignore this click.
                return;
            }
        }

        // causes a re-render
        this.setState({
            history: history,
            stepNumber: stepNumber,
            heldPiece: newHeldPiece
        });
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
        });
    }

    static calculateWin(current) {
        const squaresWithPiece = current.squares.filter(s => s !== null).length;
        return squaresWithPiece <= 1;
    }

    render() {
        const history = this.state.history;
        const currentStep = this.state.stepNumber;
        const current = history[currentStep];
        const heldPiece = this.state.heldPiece;
        const win = Level.calculateWin(current);
        const possibilities = heldPiece
            ? PieceCalculator.calculatePossibleSquares(heldPiece.i, heldPiece.piece.pieceType, current.squares)
            : null;

        const levelTitle = this.props.levelTitle;

        const winOverlay = win ? (
            <WinOverlay finishLevel={this.props.finishLevel} />
        ) : null;

        // floating piece to follow mouse movement.
        const piece = heldPiece ? (
            <div style={{position: "absolute", top: this.mouse.y, left: this.mouse.x}} data-floating-piece>
                <ChessPiece piece={heldPiece.piece} />
            </div>
        ) : null;

        return (
            <div className="game">
                <h1>{levelTitle}</h1>
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={(i, piece) => this.handleClick(i, piece)}
                        onTouch={(e, i, piece) => this.handleTouch(e, i, piece)}
                        heldPiece={heldPiece}
                        possibilities={possibilities}
                        ghosts={history[0].squares}
                    />
                </div>
                <div>
                    <Navigation
                        totalMoves={history.length}
                        history={history}
                        currentStep={currentStep}
                        navigate={(i) => this.jumpTo(i)}
                    />
                </div>
                {piece}
                {winOverlay}
            </div>
        );
    }
}

export default Level;