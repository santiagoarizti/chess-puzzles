import React from 'react';

class LevelButton extends React.Component {

    handleClick(level) {
        this.props.onClick(level);
    }

    render() {

        const level = this.props.level;
        const cleared = this.props.cleared;
        const open = this.props.open;

        let classNames = ["level-button cell shrink"];
        if (cleared) classNames.push("cleared");

        return (
            <div className={classNames.join(" ")}>
                <button onClick={() => this.handleClick(level)} disabled={!open} className="button">
                    {level + 1}
                </button>
            </div>
        );

    }

}

export default LevelButton;