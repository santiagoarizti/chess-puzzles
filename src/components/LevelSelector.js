import React from 'react';
import levels from '../levels';
import LevelButton from './LevelButton';

class LevelSelector extends React.Component {

    /**
     * Adds two new properties to level: "cleared" and "open", also groups by difficulty.
     * @returns {{beginner: Array, intermediate: Array, advanced: Array, expert: Array}}
     */
    static processLevels(levels) {

        // custom per user, updated on every level clear.
        const clearedLevels = LevelSelector.getClearedLevels();

        // grouped by difficulty
        const levelGroups = {
            beginner: { title: "Beginner", levels: [] },
            intermediate: { title: "Intermediate", levels: [] },
            advanced: { title: "Advanced", levels: [] },
            expert: { title: "Expert", levels: [] }
        };

        let maxCleared = -1;

        levels.forEach((level, i) => {

            // adding custom properties

            level.i = i;
            level.cleared = clearedLevels[i]; // either true or undefined.

            if (level.cleared) maxCleared = i;

            level.open = maxCleared + 1 >= i;

            // grouping by difficulty

            levelGroups[level.difficulty].levels.push(level);
        });

        return levelGroups;
    }

    render() {

        const processedLevels = LevelSelector.processLevels(levels);

        // will contain jsx elements.
        const levelGroups = [];

        for (const difficulty in processedLevels) {
            const levelGroup = processedLevels[difficulty];

            const levelButtons = levelGroup.levels.map(level => (
                <LevelButton level={level.i} onClick={this.props.onClick} key={level.i} cleared={level.cleared} open={level.open} />
            ));

            levelGroups.push(
                <div key={difficulty} className="grid-x grid-margin-x">
                    <h2 className="difficulty cell small-12">{levelGroup.title}</h2>
                    {levelButtons}
                </div>
            );
        }

        return (
            <div className="level-selector">
                <h1>
                    Select a level
                </h1>
                <div className="level-buttons">
                    {levelGroups}
                </div>
            </div>
        );
    }

    static getClearedLevels() {
        let clearedLevels = {};
        if (typeof(Storage) !== "undefined") {
            if (localStorage.clearedLevels) {
                clearedLevels = JSON.parse(localStorage.clearedLevels);
            }
        }
        return clearedLevels;
    }
}

export default LevelSelector;