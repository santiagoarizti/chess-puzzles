import React from 'react';
import Board from './Board';
import ChessPiece from './ChessPiece'

class Square extends React.Component{
    constructor() {
        super();
        this.state = {
            // not used for now.
        };
    }

    getSquareColor(isPossibility) {
        const pos = Board.getCoordinates(this.props.position);
        return (pos.col + pos.row) % 2 ? "SaddleBrown" : "BurlyWood";
        /*return (isPossibility)
            ? ((pos.col + pos.row) % 2 ? "#693413" : "#b48f5e")
            : ((pos.col + pos.row) % 2 ? "SaddleBrown" : "BurlyWood");*/
    }

    static handleClick(square) {
        // clicked on an square, this.props.piece can be empty (null)
        square.props.onClick(square.props.piece);
    }

    static handleTouch(e, square) {
        // clicked on an square, this.props.piece can be empty (null)
        square.props.onTouch(e, square.props.piece);
    }

    render() {
        const piece = this.props.piece
            ? (
                <ChessPiece
                    piece={this.props.piece}
                    isHeld={this.props.isHeld}
                />
            )
            : null;
        const ghostPiece = this.props.ghost
            ?(
                <ChessPiece
                    piece={this.props.ghost}
                    ghost={true}
                />
            )
            : null;

        const hl = this.props.isPossibility
            ? (
                <div className="hl" />
            )
            : null;

        const style = {
            background: this.getSquareColor(this.props.isPossibility), // chantosM0v1l
            //border: this.props.isPossibility ? "10px solid gray" : null,
        };

        return (
            <button
                className="square"
                onClick={() => Square.handleClick(this)}
                onTouchStart={(e) => Square.handleTouch(e, this)}
                onTouchEnd={(e) => Square.handleTouch(e, this)}
                style={style}
            >
                {hl}
                {ghostPiece}
                {piece}
            </button>
        );
    }
}

export default Square;
