import React from 'react';
import Square from './Square';

class Board extends React.Component {

    static getBoardSize() {
        return 4;
    }

    static getCoordinates(square) {
        const boardSize = Board.getBoardSize();
        const row = Math.floor(square / boardSize) + 1;
        const col = (square % boardSize) + 1;
        return {col: col, row: row};
    }

    static backToSingleDimension(coordinate) {
        const boardSize = Board.getBoardSize();
        return (boardSize * (coordinate.row - 1)) + (coordinate.col - 1);
    }

    static getChessCoordinates(square) {
        const pos = Board.getCoordinates(square);
        // in chess, the bottom row is lowest
        // and columns are letters.
        //const colMap = ["", "A", "B", "C", "D", "E", "F", "G", "H"];
        const colMap = ["", "a", "b", "c", "d", "e", "f", "g", "h"];
        return {col: colMap[pos.col], row: Board.getBoardSize() - pos.row + 1};
    }

    render() {
        const rowSize = Board.getBoardSize();
        let rows = [];
        for (let row = 0; row < rowSize; row++) {
            let cols = [];
            const iStart = row * rowSize;
            for (let i = iStart; i < iStart + rowSize; i ++) {

                // has heldPiece.i and heldPiece.piece
                const heldPiece = this.props.heldPiece;
                const isHeld = heldPiece && heldPiece.i === i;
                const isPossibility = this.props.possibilities
                    ? !!this.props.possibilities[i]
                    : false;

                cols.push(
                    <Square
                        key={i}
                        piece={this.props.squares[i]}
                        onClick={(piece) => this.props.onClick(i, piece)}
                        onTouch={(e, piece) => this.props.onTouch(e, i, piece)}
                        position={i}
                        isHeld={isHeld}
                        isPossibility={isPossibility}
                        ghost={this.props.ghosts[i]}
                    />
                );
            }
            rows.push(
                <div key={row} className="board-row">
                    {cols}
                </div>
            );
        }

        return (
            <div>
                {rows}
            </div>
        );
    }
}

export default Board;