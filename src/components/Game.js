import React from 'react';
import Level from './Level';
import LevelSelector from './LevelSelector';
import levels from '../levels';

class Game extends React.Component {

    constructor() {
        super();

        this.state = {
            level: null
        };
    }

    selectLevel(level) {
        this.setState({
            level: level
        });
    }

    finishLevel() {
        this.setState({
            level: null
        });
        if (typeof(Storage) !== "undefined") {
            let clearedLevels = JSON.parse(localStorage.clearedLevels || "{}");
            clearedLevels[this.state.level] = true;
            localStorage.clearedLevels = JSON.stringify(clearedLevels);
        }
    }

    render() {

        const current = this.state;

        let toReturn;
        if (current.level !== null) {
            const levelTitle = levels[current.level].title;
            toReturn = (
                <Level
                    level={current.level}
                    finishLevel={() => this.finishLevel()}
                    levelTitle={levelTitle}
                />
            )
        } else {
            toReturn = (
                <LevelSelector onClick={(level) => this.selectLevel(level)} />
            )
        }

        return toReturn;
    }

}

export default Game;