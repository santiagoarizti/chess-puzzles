export default [
    {
        title: "Level 1",
        difficulty: "beginner",
        pieces: [
            "oNoo",
            "ooBo",
            "oRoo",
            "iooo"
        ].join("")
    },
    {
        title: "Level 2",
        difficulty: "beginner",
        pieces: [
            "oRNo",
            "Boio",
            "oooo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 3",
        difficulty: "beginner",
        pieces: [
            "oRoo",
            "ooio",
            "oBoo",
            "Nooo"
        ].join("")
    },
    {
        title: "Level 4",
        difficulty: "beginner",
        pieces: [
            "oNoo",
            "ooio",
            "Booo",
            "oRoo"
        ].join("")
    },
    {
        title: "Level 5",
        difficulty: "beginner",
        pieces: [
            "oooo",
            "RooN",
            "oNoo",
            "Booo"
        ].join("")
    },
    {
        title: "Level 6",
        difficulty: "beginner",
        pieces: [
            "RoNo",
            "oooo",
            "oNoo",
            "Booo"
        ].join("")
    },
    {
        title: "Level 7",
        difficulty: "beginner",
        pieces: [
            "oooo",
            "oRoN",
            "BNoo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 8",
        difficulty: "beginner",
        pieces: [
            "oooo",
            "oNoo",
            "oRoN",
            "Booo"
        ].join("")
    },
    {
        title: "Level 9",
        difficulty: "beginner",
        pieces: [
            "ooRo",
            "Kioo",
            "Nooo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 10",
        difficulty: "beginner",
        pieces: [
            "oNoo",
            "iooR",
            "oKoo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 11",
        difficulty: "beginner",
        pieces: [
            "RoKo",
            "oioo",
            "oooo",
            "Nooo"
        ].join("")
    },
    {
        title: "Level 12",
        difficulty: "beginner",
        pieces: [
            "ooKo",
            "oRoo",
            "ioNo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 13",
        difficulty: "beginner",
        pieces: [
            "oNoo",
            "oQoR",
            "iooo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 14",
        difficulty: "beginner",
        pieces: [
            "ooQo",
            "iooo",
            "ooNo",
            "Rooo"
        ].join("")
    },
    {
        title: "Level 15",
        difficulty: "beginner",
        pieces: [
            "QoRo",
            "oooo",
            "oNoo",
            "iooo"
        ].join("")
    },
    {
        title: "Level 16",
        difficulty: "beginner",
        pieces: [
            "ooQo",
            "ooRo",
            "Nooo",
            "oioo"
        ].join("")
    },
    {
        title: "Level 17",
        difficulty: "beginner",
        pieces: [
            "iooo",
            "oNoo",
            "oKRo",
            "ooBo"
        ].join("")
    },
    {
        title: "Level 18",
        difficulty: "beginner",
        pieces: [
            "oKoo",
            "ooRi",
            "oBoo",
            "Nooo"
        ].join("")
    },
    {
        title: "Level 19",
        difficulty: "beginner",
        pieces: [
            "ooio",
            "NKoo",
            "oBoo",
            "Rooo"
        ].join("")
    },
    {
        title: "Level 20",
        difficulty: "beginner",
        pieces: [
            "ooNo",
            "oioo",
            "ooKo",
            "RBoo"
        ].join("")
    },
    {
        title: "Level 21",
        difficulty: "intermediate",
        pieces: [
            "Qooo",
            "oNoR",
            "ooBo",
            "iooo"
        ].join("")
    },
    {
        title: "Level 22",
        difficulty: "intermediate",
        pieces: [
            "QoRo",
            "oooB",
            "ooNo",
            "iooo"
        ].join("")
    },
    {
        title: "Level 23",
        difficulty: "intermediate",
        pieces: [
            "oQoi",
            "oNoo",
            "BoRo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 24",
        difficulty: "intermediate",
        pieces: [
            "Rooo",
            "ooQi",
            "NBoo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 25",
        difficulty: "intermediate",
        pieces: [
            "Rooo",
            "oiNo",
            "oooo",
            "RoNo"
        ].join("")
    },
    {
        title: "Level 26",
        difficulty: "intermediate",
        pieces: [
            "oRoN",
            "oNoo",
            "ioRo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 27",
        difficulty: "intermediate",
        pieces: [
            "ooRo",
            "oNoR",
            "ooio",
            "Nooo"
        ].join("")
    },
    {
        title: "Level 28",
        difficulty: "intermediate",
        pieces: [
            "Rooo",
            "ooNo",
            "Nooo",
            "oiRo"
        ].join("")
    },
    {
        title: "Level 29",
        difficulty: "intermediate",
        pieces: [
            "QRoo",
            "oRoo",
            "ooio",
            "Booo"
        ].join("")
    },
    {
        title: "Level 30",
        difficulty: "intermediate",
        pieces: [
            "QBoo",
            "Rooo",
            "oooi",
            "Rooo"
        ].join("")
    },
    {
        title: "Level 31",
        difficulty: "intermediate",
        pieces: [
            "oRoo",
            "ooio",
            "oBoo",
            "QooR"
        ].join("")
    },
    {
        title: "Level 32",
        difficulty: "intermediate",
        pieces: [
            "ooRo",
            "QooB",
            "ooio",
            "Rooo"
        ].join("")
    },
    {
        title: "Level 33",
        difficulty: "intermediate",
        pieces: [
            "RNoo",
            "Kooo",
            "oBoo",
            "Noio"
        ].join("")
    },
    {
        title: "Level 34",
        difficulty: "intermediate",
        pieces: [
            "NRoo",
            "oKoN",
            "ioBo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 35",
        difficulty: "intermediate",
        pieces: [
            "NoBo",
            "KioN",
            "Rooo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 36",
        difficulty: "intermediate",
        pieces: [
            "Nooo",
            "oKBo",
            "Rooi",
            "Nooo"
        ].join("")
    },
    {
        title: "Level 37",
        difficulty: "intermediate",
        pieces: [
            "oooo",
            "oioo",
            "ooio",
            "oooo"
        ].join("")
    },
    {
        title: "Level 38",
        difficulty: "intermediate",
        pieces: [
            "KooQ",
            "oooo",
            "oooo",
            "RooB"
        ].join("")
    },
    {
        title: "Level 39",
        difficulty: "advanced",
        pieces: [
            "oooR",
            "oRBo",
            "oNio",
            "iooo"
        ].join("")
    },
    {
        title: "Level 40",
        difficulty: "advanced",
        pieces: [
            "oBoR",
            "ooRo",
            "oioo",
            "ioNo"
        ].join("")
    },
    {
        title: "Level 41",
        difficulty: "advanced",
        pieces: [
            "ooBR",
            "ooBR",
            "oooi",
            "Nooo"
        ].join("")
    },
    {
        title: "Level 42",
        difficulty: "advanced",
        pieces: [
            "ooRN",
            "ioRo",
            "oBoo",
            "ooBo"
        ].join("")
    },
    {
        title: "Level 43",
        difficulty: "advanced",
        pieces: [
            "ooRo",
            "BBRN",
            "oooo",
            "oioo"
        ].join("")
    },
    {
        title: "Level 44",
        difficulty: "advanced",
        pieces: [
            "Rooo",
            "BoBo",
            "oRoN",
            "iooo"
        ].join("")
    },
    {
        title: "Level 45",
        difficulty: "advanced",
        pieces: [
            "oRoN",
            "Noio",
            "oBoo",
            "ooio"
        ].join("")
    },
    {
        title: "Level 46",
        difficulty: "advanced",
        pieces: [
            "oooN",
            "oBoN",
            "oRio",
            "iooo"
        ].join("")
    },
    {
        title: "Level 47",
        difficulty: "advanced",
        pieces: [
            "oooi",
            "oNBo",
            "oooN",
            "Roio"
        ].join("")
    },
    {
        title: "Level 48",
        difficulty: "advanced",
        pieces: [
            "ooNo",
            "NoBo",
            "oRoi",
            "iooo"
        ].join("")
    },
    {
        title: "Level 49",
        difficulty: "advanced",
        pieces: [
            "oNoB",
            "Nooo",
            "oRiB",
            "iooo"
        ].join("")
    },
    {
        title: "Level 50",
        difficulty: "advanced",
        pieces: [
            "ooBN",
            "oRBi",
            "Booo",
            "oioo"
        ].join("")
    },
    {
        title: "Level 51",
        difficulty: "advanced",
        pieces: [
            "oNoo",
            "ooNB",
            "oRBi",
            "iooo"
        ].join("")
    },
    {
        title: "Level 52",
        difficulty: "advanced",
        pieces: [
            "BooR",
            "oiio",
            "oNBo",
            "Nooo"
        ].join("")
    },
    {
        title: "Level 53",
        difficulty: "advanced",
        pieces: [
            "oBoo",
            "NBoo",
            "oKNo",
            "iooi"
        ].join("")
    },
    {
        title: "Level 54",
        difficulty: "advanced",
        pieces: [
            "oooi",
            "ooBN",
            "BKoo",
            "oNio"
        ].join("")
    },
    {
        title: "Level 55",
        difficulty: "advanced",
        pieces: [
            "ooBo",
            "KNoo",
            "iooB",
            "ioNo"
        ].join("")
    },
    {
        title: "Level 56",
        difficulty: "advanced",
        pieces: [
            "ioiN",
            "oBKo",
            "oBNo",
            "oooo"
        ].join("")
    },
    {
        title: "Level 57",
        difficulty: "advanced",
        pieces: [
            "oooi",
            "BNRo",
            "BoRo",
            "ooio"
        ].join("")
    },
    {
        title: "Level 58",
        difficulty: "advanced",
        pieces: [
            "BNoo",
            "iRoB",
            "ooRo",
            "ooio"
        ].join("")
    },
    {
        title: "Level 59",
        difficulty: "advanced",
        pieces: [
            "Rooi",
            "oBRo",
            "BooN",
            "ooio"
        ].join("")
    },
    {
        title: "Level 60",
        difficulty: "advanced",
        pieces: [
            "ooBB",
            "oNKo",
            "oooi",
            "iRoo"
        ].join("")
    },
    {
        title: "Level 61",
        difficulty: "expert",
        pieces: [
            "ooiR",
            "oooN",
            "RoNo",
            "Bioo"
        ].join("")
    },
    {
        title: "Level 62",
        difficulty: "expert",
        pieces: [
            "ooRN",
            "oooN",
            "BoRo",
            "iioo"
        ].join("")
    },
    {
        title: "Level 63",
        difficulty: "expert",
        pieces: [
            "oooi",
            "oRoN",
            "oooR",
            "NBio"
        ].join("")
    },
    {
        title: "Level 64",
        difficulty: "expert",
        pieces: [
            "oooi",
            "oRoR",
            "oooN",
            "NBio"
        ].join("")
    },
    {
        title: "Level 65",
        difficulty: "expert",
        pieces: [
            "oRoN",
            "Boio",
            "NKoo",
            "oBio"
        ].join("")
    },
    {
        title: "Level 66",
        difficulty: "expert",
        pieces: [
            "iBoo",
            "NNoo",
            "KoBi",
            "Rooo"
        ].join("")
    },
    {
        title: "Level 67",
        difficulty: "expert",
        pieces: [
            "oRio",
            "KNoi",
            "NooB",
            "Booo"
        ].join("")
    },
    {
        title: "Level 68",
        difficulty: "expert",
        pieces: [
            "oKBi",
            "oBoN",
            "BRoo",
            "iooo"
        ].join("")
    },
    {
        title: "Level 69",
        difficulty: "expert",
        pieces: [
            "ooRi",
            "oBNo",
            "oNRo",
            "iBoo"
        ].join("")
    },
    {
        title: "Level 70",
        difficulty: "expert",
        pieces: [
            "ooRi",
            "oiRo",
            "oNBo",
            "BNoo"
        ].join("")
    },
    {
        title: "Level 71",
        difficulty: "expert",
        pieces: [
            "ooNi",
            "ooBi",
            "RRoo",
            "BNoo"
        ].join("")
    },
    {
        title: "Level 72",
        difficulty: "expert",
        pieces: [
            "ooRN",
            "ooBN",
            "iRoo",
            "Bioo"
        ].join("")
    },
    {
        title: "Level 73",
        difficulty: "expert",
        pieces: [
            "oioo",
            "ooQR",
            "RoBo",
            "ioBN"
        ].join("")
    },
    {
        title: "Level 74",
        difficulty: "expert",
        pieces: [
            "oiQo",
            "oooi",
            "RoBo",
            "BoRN"
        ].join("")
    },
    {
        title: "Level 75",
        difficulty: "expert",
        pieces: [
            "RRoo",
            "NQBi",
            "Booo",
            "ooio"
        ].join("")
    },
    {
        title: "Level 76",
        difficulty: "expert",
        pieces: [
            "oBoo",
            "oRoi",
            "iQoo",
            "RNBo"
        ].join("")
    },
    {
        title: "Level 77",
        difficulty: "expert",
        pieces: [
            "oBio",
            "NooR",
            "ooio",
            "BoRN"
        ].join("")
    }
];
