import React from 'react';
import PieceCalculator from '../PieceCalculator';

const emptyBoard = Array(16).fill();

test('can calculate bishop', () => {
    /*
    Bishop up-right    Bishop middle
    +---+---+---+---+  +---+---+---+---+
    |   |   |   | o |  |   | o |   | o |
    +---+---+---+---+  +---+---+---+---+
    |   |   | o |   |  |   |   | B |   |
    +---+---+---+---+  +---+---+---+---+
    |   | o |   |   |  |   | o |   | o |
    +---+---+---+---+  +---+---+---+---+
    | B |   |   |   |  | o |   |   |   |
    +---+---+---+---+  +---+---+---+---+*/

    const possibilities1 = PieceCalculator.calculateBishopPossibilities(
        { row: 4, col: 1 }, emptyBoard, false
    );
    const possibilities2 = PieceCalculator.calculateBishopPossibilities(
        { row: 2, col: 3 }, emptyBoard, false
    );

    expect(possibilities1).toEqual({"3": true, "6": true, "9": true});
    expect(possibilities2).toEqual({"1": true, "3": true, "9": true, "11": true, "12": true});
});

test('can calculate bishop with other pieces', () => {
    /*
    Bishop up-right    Bishop middle
    +---+---+---+---+  +---+---+---+---+
    |   |   |   |   |  |   | o |   | o |
    +---+---+---+---+  +---+---+---+---+
    |   |   |   |   |  |   |   | B |   |
    +---+---+---+---+  +---+---+---+---+
    |   | i |   |   |  |   | i |   | o |
    +---+---+---+---+  +---+---+---+---+
    | B |   |   |   |  |   |   |   |   |
    +---+---+---+---+  +---+---+---+---+*/

    const busyBoard = emptyBoard.slice(0);
    busyBoard[9] = true;

    const possibilities1 = PieceCalculator.calculateBishopPossibilities(
        { row: 4, col: 1 }, busyBoard, false
    );
    const possibilities2 = PieceCalculator.calculateBishopPossibilities(
        { row: 2, col: 3 }, busyBoard, false
    );

    expect(possibilities1).toEqual({"9": true});
    expect(possibilities2).toEqual({"1": true, "3": true, "9": true, "11": true});
});

test('can calculate rook', () => {
    /*
    Rook up-right      Rook middle
    +---+---+---+---+  +---+---+---+---+
    | o |   |   |   |  |   |   | o |   |
    +---+---+---+---+  +---+---+---+---+
    | o |   |   |   |  | o | o | R | o |
    +---+---+---+---+  +---+---+---+---+
    | o |   |   |   |  |   |   | o |   |
    +---+---+---+---+  +---+---+---+---+
    | R | o | o | o |  |   |   | o |   |
    +---+---+---+---+  +---+---+---+---+*/

    const possibilities1 = PieceCalculator.calculateRookPossibilities(
        { row: 4, col: 1 }, emptyBoard, false
    );
    const possibilities2 = PieceCalculator.calculateRookPossibilities(
        { row: 2, col: 3 }, emptyBoard, false
    );

    expect(possibilities1).toEqual({"0": true, "4": true, "8": true, "13": true, "14": true, "15": true});
    expect(possibilities2).toEqual({"2": true, "4": true, "5": true, "7": true, "10": true, "14": true});
});

test('can calculate rook with other pieces', () => {
    /*
    Rook up-right      Rook middle
    +---+---+---+---+  +---+---+---+---+
    |   |   |   |   |  |   |   | o |   |
    +---+---+---+---+  +---+---+---+---+
    | i |   |   |   |  | i | o | R | o |
    +---+---+---+---+  +---+---+---+---+
    | o |   |   |   |  |   |   | o |   |
    +---+---+---+---+  +---+---+---+---+
    | R | o | o | o |  |   |   | o |   |
    +---+---+---+---+  +---+---+---+---+*/

    const busyBoard = emptyBoard.slice(0);
    busyBoard[4] = true;

    const possibilities1 = PieceCalculator.calculateRookPossibilities(
        { row: 4, col: 1 }, busyBoard, false
    );
    const possibilities2 = PieceCalculator.calculateRookPossibilities(
        { row: 2, col: 3 }, busyBoard, false
    );

    expect(possibilities1).toEqual({"4": true, "8": true, "13": true, "14": true, "15": true});
    expect(possibilities2).toEqual({"2": true, "4": true, "5": true, "7": true, "10": true, "14": true});
});

test('can calculate knight', () => {
    /* Knight
    +---+---+---+---+
    | x |   |   |   |
    +---+---+---+---+
    | x |   | N |   |
    +---+---+---+---+
    | x |   |   |   |
    +---+---+---+---+
    |   | x | x | x |
    +---+---+---+---+*/

    const possibilities1 = PieceCalculator.calculateKnightPossibilities(
        { row: 2, col: 3 }, emptyBoard, false
    );

    expect(possibilities1).toEqual({"0": true, "8": true, "13": true, "15": true});
});

test('can calculate pawn', () => {
    /* Pawn            Pawn with pieces
    +---+---+---+---+  +---+---+---+---+
    |   |   | x |   |  |   | Q | K |   |
    +---+---+---+---+  +---+---+---+---+
    |   |   | i |   |  |   |   | i |   |
    +---+---+---+---+  +---+---+---+---+
    |   |   |   |   |  |   |   |   |   |
    +---+---+---+---+  +---+---+---+---+
    |   |   |   |   |  |   |   |   |   |
    +---+---+---+---+  +---+---+---+---+*/

    const busyBoard = emptyBoard.slice(0);
    busyBoard[1] = true;
    busyBoard[2] = true;

    const possibilities1 = PieceCalculator.calculatePawnPossibilities(
        { row: 2, col: 3 }, emptyBoard, false
    );

    const possibilities2 = PieceCalculator.calculatePawnPossibilities(
        { row: 2, col: 3 }, busyBoard, false
    );

    expect(possibilities1).toEqual({"2": true});
    expect(possibilities2).toEqual({"1": true});
});