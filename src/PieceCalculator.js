import Board from './components/Board';

class PieceCalculator {
    
    /**
     * Return associative object with keys being possible landing locations based on current board.
     * @param {int} i  current single-dimensional position of the piece.
     * @param {string} pieceType  single-letter string to determine the type of piece R|B|N|Q|K|i
     * @param currentBoard  current single-dimensional taken positions on the board.
     */
    static calculatePossibleSquares(i, pieceType, currentBoard) {
        /*  1   2   3   4    Rook               Bishop             Knight             Queen/King         Pawn
          +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+
        1 | 0 | 1 | 2 | 3 |  |   | o |   |   |  |   |   |   | o |  | o |   | o |   |  |   | o |   | o |  |   |   |   |   |
          +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+
        2 | 4 | 5 | 6 | 7 |  |   | o |   |   |  | o |   | o |   |  |   |   |   | o |  | o | o | o |   |  | x | o | x |   |
          +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+
        3 | 8 | 9 |10 |11 |  | o | R | o | o |  |   | B |   |   |  |   | N |   |   |  | o | Q | o | o |  |   | i |   |   |
          +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+
        4 |12 |13 |14 |15 |  |   | o |   |   |  | o |   | o |   |  |   |   |   | o |  | o | o | o |   |  |   |   |   |   |
          +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+  +---+---+---+---+ */
          const current = Board.getCoordinates(i); // row, col
          const possibilities = {};
          switch (pieceType) {
              case "R" : // Rook
                  Object.assign(possibilities, PieceCalculator.calculateRookPossibilities(current, currentBoard));
                  break;
              case "B" : // Bishop
                  Object.assign(possibilities, PieceCalculator.calculateBishopPossibilities(current, currentBoard));
                  break;
              case "Q" : // Queen, just add Rook and Bishop possibilities.
                  Object.assign(possibilities, PieceCalculator.calculateRookPossibilities(current, currentBoard),
                    PieceCalculator.calculateBishopPossibilities(current, currentBoard));
                  break;
              case "K" : // King, similar to Queen, but special rule applies.
                  Object.assign(possibilities, PieceCalculator.calculateRookPossibilities(current, currentBoard, true),
                    PieceCalculator.calculateBishopPossibilities(current, currentBoard, true));
                  break;
              case "N" : // King, similar to Queen, but special rule applies.
                  Object.assign(possibilities, PieceCalculator.calculateKnightPossibilities(current));
                  break;
              case "i" : // King, similar to Queen, but special rule applies.
                  Object.assign(possibilities, PieceCalculator.calculatePawnPossibilities(current, currentBoard));
                  break;
              default :
                  // no possible moves for unknown pieces
                  break;
          }
  
          // special case for these puzzles: the square must have a piece.
          let specialPossibilities = {};
          for (let a in possibilities) {
              if (currentBoard[a]) specialPossibilities[a] = true;
          }
  
          return specialPossibilities;
      }
  
      static calculateRookPossibilities(p, currentBoard, king) {
          const boardSize = Board.getBoardSize();
          let possibilities = {};
          // whenever we see that a piece is in a possible landing position, or we are talking about the king,
          // we break and stop looking in that direction.
          for (let col = p.col + 1; col <= boardSize; col++) { // same row, to the right.
              const possibility = Board.backToSingleDimension({ row: p.row, col: col });
              possibilities[possibility] = true;
              if (currentBoard[possibility] || king) break;
          }
          for (let col = p.col - 1; col >= 1; col--) { // same row, to the left.
              const possibility = Board.backToSingleDimension({ row: p.row, col: col });
              possibilities[possibility] = true;
              if (currentBoard[possibility] || king) break;
          }
          for (let row = p.row + 1; row <= boardSize; row++) { // same column, to the right.
              const possibility = Board.backToSingleDimension({ row: row, col: p.col});
              possibilities[possibility] = true;
              if (currentBoard[possibility] || king) break;
          }
          for (let row = p.row - 1; row >= 1; row--) { // same column, to the left.
              const possibility = Board.backToSingleDimension({ row: row, col: p.col});
              possibilities[possibility] = true;
              if (currentBoard[possibility] || king) break;
          }
  
          return possibilities;
      }
  
      static calculateBishopPossibilities(p, currentBoard, king) {
          const boardSize = Board.getBoardSize();
          let possibilities = {};

          /** @param {Object} u stands for unit-vector. e.g. {row: 1, col: 1} | {row: 1, col: -1} */
          const iterate = (u) => { //
              // whenever we see that a piece is in a possible landing position, or we are talking about the king,
              // we break and stop looking in that direction.
              for (let offset = 1; offset <= boardSize; offset++) { // up-right
                  const i = {row: p.row + (offset * u.row), col: p.col + (offset * u.col)};
                  if (PieceCalculator.withinBounds(i, boardSize)) {
                      const possibility = Board.backToSingleDimension(i);
                      possibilities[possibility] = true;
                      if (currentBoard[possibility] || king) break;
                  } else break;
              }
          };

          // up-right
          iterate({ row: 1, col: 1 });
          // up-left
          iterate({ row: 1, col: -1 });
          // down-right
          iterate({ row: -1, col: 1 });
          // down-left
          iterate({ row: -1, col: -1 });
  
          return possibilities;
      }
  
      static calculateKnightPossibilities(p) {
          const boardSize = Board.getBoardSize();
          let possibilities = {};
  
          // all possible knight locations, in an infinite board
          const fullDomain = [
              { row: p.row + 1, col: p.col + 2 },
              { row: p.row + 2, col: p.col + 1 },
              { row: p.row - 1, col: p.col + 2 },
              { row: p.row - 2, col: p.col + 1 },
              { row: p.row + 1, col: p.col - 2 },
              { row: p.row + 2, col: p.col - 1 },
              { row: p.row - 1, col: p.col - 2 },
              { row: p.row - 2, col: p.col - 1 },
          ];
  
          // trim and convert and add to our possibilities.
          fullDomain.filter(i =>
                  i.row <= boardSize && i.row >= 1 && i.col <= boardSize && i.col >= 1
              )
              .map(Board.backToSingleDimension)
              .forEach(i => possibilities[i] = true);
  
          return possibilities;
      }
  
      static calculatePawnPossibilities(p, currentBoard) {
          const boardSize = Board.getBoardSize();
          let possibilities = {};
  
          // pawn is not symmetrical like the others, so the y axis is confusing.
          const fullDomain = [
              { row: p.row - 1, col: p.col - 1 }, // to eat
              { row: p.row - 1, col: p.col, normal: true }, // normal
              { row: p.row - 1, col: p.col + 1 }, // to eat
              //{ row: p.row - 2, col: p.col }, // first move only.
          ];
  
          // trim and convert and add to our possibilities.
          fullDomain.filter(i => PieceCalculator.withinBounds(i, boardSize))
              .filter(i => {
                  // filter to see if the cell has or not a piece in the way, or to eat
                  const possibility = Board.backToSingleDimension(i);
                  return (i.normal && !currentBoard[possibility]) || (!i.normal && currentBoard[possibility])
              })
              .map(Board.backToSingleDimension)
              .forEach(i => possibilities[i] = true);
  
          return possibilities;
      }

      static withinBounds(i, boardSize) {
          // normal filter to the bounds of the board
          return i.row <= boardSize && i.row >= 1 && i.col <= boardSize && i.col >= 1
      }
}

export default PieceCalculator;